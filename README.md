# Slack

This is a thin library for interacting with Slack. Currently, it supports sending messages and uploading files. 

## Installing

* Run:

```
composer require talentrydev/slack
```

## Usage example

```
$apiToken = ''; //generate the token in slack
$guzzleClient = new \GuzzleHttp\Client();
$slack = new \Talentry\Slack\SlackClient($apiToken, $guzzleClient);

$channel = $slack->getChannel('general');
if ($slackChannel !== null) {
    $slack->sendPlainTextMessage('Hello world!', $slackChannel);
}
```

## Development

- Install dependencies: `make deps`
- Run tests: `make test`
