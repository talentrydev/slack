<?php

declare(strict_types=1);

namespace Talentry\Slack\Tests;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SplFileInfo;
use Talentry\Slack\Channel;
use Talentry\Slack\Exception\SlackApiException;
use Talentry\Slack\MessageType;
use Talentry\Slack\Resource;
use Talentry\Slack\SlackClient;
use Talentry\Slack\Tests\Mocks\MockGuzzleClient;
use Throwable;

class SlackClientTest extends TestCase
{
    private SlackClient $slackClient;
    private MockGuzzleClient $mockGuzzleClient;
    private string $slackApiToken = 'token';
    private string $plainTextMessage = 'foo';
    private string $markdownMessage = '*foo*';
    private MessageType $type;
    private ?string $message = null;
    private string $existingChannelName = 'foo';
    private string $existingChannelNameOnSecondPage = 'baz';
    private string $nonExistingChannelName = 'unknown';
    private Channel $targetChannel;
    private ?Channel $fetchedChannel = null;
    private ?Throwable $caughtException = null;
    private SplFileInfo $file;
    private string $apiErrorResponse;
    private string $slackFileId = 'F072ADFH47R'; //from fixtures
    private string $slackFileUrl = 'https://example.org'; //from fixtures

    protected function setUp(): void
    {
        $this->mockGuzzleClient = new MockGuzzleClient();
        $this->targetChannel = new Channel('FOO', 'foo');
        $this->file = new SplFileInfo(realpath(__FILE__));
        $this->slackClient = new SlackClient($this->slackApiToken, $this->mockGuzzleClient);
        $this->apiErrorResponse = json_encode([
            'ok' => false,
            'error' => 'Internal server error',
        ]);
    }

    protected function tearDown(): void
    {
        if ($this->caughtException !== null) {
            throw $this->caughtException;
        }
    }

    public function testSendPlainTextMessage(): void
    {
        $this->whenPlainTextMessageIsSentToTargetChannel();
        $this->expectMessageToBePostedToTargetChannel();
    }

    public function testSendMarkdownMessage(): void
    {
        $this->whenMarkdownMessageIsSentToTargetChannel();
        $this->expectMessageToBePostedToTargetChannel();
    }

    public function testGetExistingChannel(): void
    {
        $this->givenThatApiWillReturnAListOfChannels();
        $this->whenChannelIsFetched($this->existingChannelName);
        $this->expectChannelToBeFetched($this->existingChannelName);
    }

    public function testGetExistingChannelOnSecondPage(): void
    {
        $this->givenThatApiWillReturnAListOfChannels();
        $this->whenChannelIsFetched($this->existingChannelNameOnSecondPage);
        $this->expectChannelToBeFetched($this->existingChannelNameOnSecondPage);
    }

    public function testGetNonExistingChannel(): void
    {
        $this->givenThatApiWillReturnAListOfChannels();
        $this->whenChannelIsFetched($this->nonExistingChannelName);
        $this->expectNoChannelToBeFetched();
    }

    public function testGetExistingChannelWithInvalidApiResponse(): void
    {
        $this->givenInvalidApiResponse();
        $this->whenChannelIsFetched($this->existingChannelName);
        $this->expectInvalidApiResponseError();
    }

    #[DataProvider('dataProviderForTestSendFile')]
    public function testSendFile(bool $withMessage): void
    {
        if ($withMessage) {
            $this->message = $this->plainTextMessage;
        }

        //client will make 3 requests, so we mock a response for each
        $this->givenThatApiWillReturnAnUploadUrlResponse();
        $this->givenThatApiWillReturnAnOkResponse();
        $this->givenThatApiWillReturnACompleteUploadResponse();
        $this->whenFileIsSent();
        $this->expectFileWasSent();
    }

    public static function dataProviderForTestSendFile(): array
    {
        return [
            [true],
            [false],
        ];
    }

    public function testSendFileWithInvalidApiResponse(): void
    {
        $this->givenInvalidApiResponse();
        $this->whenFileIsSent();
        $this->expectInvalidApiResponseError();
    }

    private function givenThatApiWillReturnAListOfChannels(): void
    {
        $page1 = file_get_contents(__DIR__ . '/Fixtures/channels.response.page.1.json');
        $page2 = file_get_contents(__DIR__ . '/Fixtures/channels.response.page.2.json');
        $this->mockGuzzleClient->appendMockResponse(new Response(200, [], $page1));
        $this->mockGuzzleClient->appendMockResponse(new Response(200, [], $page2));
    }

    private function givenThatApiWillReturnAnUploadUrlResponse(): void
    {
        $response = file_get_contents(__DIR__ . '/Fixtures/files.getUploadURLExternal.response.json');
        $this->mockGuzzleClient->appendMockResponse(new Response(200, [], $response));
    }

    private function givenThatApiWillReturnACompleteUploadResponse(): void
    {
        $response = file_get_contents(__DIR__ . '/Fixtures/files.completeUpload.response.json');
        $this->mockGuzzleClient->appendMockResponse(new Response(200, [], $response));
    }

    private function givenThatApiWillReturnAnOkResponse(): void
    {
        $this->mockGuzzleClient->appendMockResponse(new Response());
    }

    private function givenInvalidApiResponse(): void
    {
        $this->mockGuzzleClient->appendMockResponse(new Response(200, [], $this->apiErrorResponse));
    }

    private function whenPlainTextMessageIsSentToTargetChannel(): void
    {
        try {
            $this->slackClient->sendPlainTextMessage($this->plainTextMessage, $this->targetChannel);
        } catch (Throwable $e) {
            $this->caughtException = $e;
        }

        $this->message = $this->plainTextMessage;
        $this->type = MessageType::TEXT;
    }

    private function whenMarkdownMessageIsSentToTargetChannel(): void
    {
        try {
            $this->slackClient->sendMarkdownMessage($this->markdownMessage, $this->targetChannel);
        } catch (Throwable $e) {
            $this->caughtException = $e;
        }

        $this->message = $this->markdownMessage;
        $this->type = MessageType::MARKDOWN;
    }

    private function whenChannelIsFetched(string $channelName): void
    {
        try {
            $this->fetchedChannel = $this->slackClient->getChannel($channelName);
        } catch (Throwable $e) {
            $this->caughtException = $e;
        }
    }

    private function whenFileIsSent(): void
    {
        try {
            $this->slackClient->sendFile($this->file, $this->targetChannel, $this->message);
        } catch (Throwable $e) {
            $this->caughtException = $e;
        }
    }

    private function expectMessageToBePostedToTargetChannel(): void
    {
        $capturedRequest = $this->mockGuzzleClient->getCapturedRequests()[0];
        self::assertSame('https://slack.com/api/chat.postMessage', $capturedRequest['uri']);
        self::assertSame('POST', $capturedRequest['method']);
        $expectedPayload = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->slackApiToken,
            ],
            'json' => [
                'channel' => $this->targetChannel->id(),
                'type' => $this->type->value,
                'text' => $this->message,
            ],
        ];
        self::assertSame($expectedPayload, $capturedRequest['options']);
    }

    private function expectChannelToBeFetched(string $channelName): void
    {
        self::assertNotNull($this->fetchedChannel);
        self::assertSame($channelName, $this->fetchedChannel->name());
    }

    private function expectNoChannelToBeFetched(): void
    {
        self::assertNull($this->fetchedChannel);
    }

    private function expectInvalidApiResponseError(): void
    {
        self::assertInstanceOf(SlackApiException::class, $this->caughtException);
        self::assertSame(
            'Invalid Slack API response: ' . $this->apiErrorResponse,
            $this->caughtException->getMessage(),
        );
        $this->caughtException = null;
    }

    private function expectFileWasSent(): void
    {
        $requests = $this->mockGuzzleClient->getCapturedRequests();
        self::assertSame('GET', $requests[0]['method']);
        $expectedQueryParams = [
            'length' => $this->file->getSize(),
            'filename' => $this->file->getBasename(),
        ];
        $expectedUrl = Resource::FILES_GET_UPLOAD_URL->url() . '?' . http_build_query($expectedQueryParams);
        self::assertSame($expectedUrl, $requests[0]['uri']);

        self::assertSame('POST', $requests[1]['method']);
        self::assertSame($this->slackFileUrl, $requests[1]['uri']);
        self::assertSame($this->file->getBasename(), $requests[1]['options']['multipart'][0]['name']);

        self::assertSame('POST', $requests[2]['method']);
        self::assertSame(Resource::FILES_COMPLETE_UPLOAD->url(), $requests[2]['uri']);
        self::assertSame($this->slackFileId, $requests[2]['options']['json']['files'][0]['id']);
        self::assertSame($this->targetChannel->id(), $requests[2]['options']['json']['channel_id']);
        if ($this->message !== null) {
            self::assertSame($this->message, $requests[2]['options']['json']['initial_comment']);
        }
    }
}
