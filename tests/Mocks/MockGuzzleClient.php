<?php

declare(strict_types=1);

namespace Talentry\Slack\Tests\Mocks;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use SplStack;

class MockGuzzleClient implements ClientInterface
{
    public function __construct(
        private array $requests = [],
        private SplStack $mockResponses = new SplStack(),
    ) {
    }

    public function send(RequestInterface $request, array $options = []): ResponseInterface
    {
        $this->requests[] = [
            'request' => $request,
            'options' => $options,
        ];

        return $this->generateResponse();
    }

    public function sendAsync(RequestInterface $request, array $options = []): PromiseInterface
    {
        $this->requests[] = [
            'request' => $request,
            'options' => $options,
        ];

        return new Promise();
    }

    public function request($method, $uri, array $options = []): ResponseInterface
    {
        $this->requests[] = [
            'method' => $method,
            'uri' => $uri,
            'options' => $options,
        ];

        return $this->generateResponse();
    }

    public function requestAsync($method, $uri, array $options = []): PromiseInterface
    {
        $this->requests[] = [
            'method' => $method,
            'uri' => $uri,
            'options' => $options,
        ];

        return new Promise();
    }

    public function getConfig($option = null)
    {
        return null;
    }

    public function getCapturedRequests(): array
    {
        return $this->requests;
    }

    public function appendMockResponse(Response $response): void
    {
        $this->mockResponses->push($response);
    }

    private function generateResponse(): Response
    {
        if ($this->mockResponses->isEmpty()) {
            return new Response();
        }

        return $this->mockResponses->shift();
    }
}
