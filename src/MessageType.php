<?php

declare(strict_types=1);

namespace Talentry\Slack;

enum MessageType: string
{
    case TEXT = 'text';
    case MARKDOWN = 'mrkdwn';

    public static function default(): self
    {
        return self::TEXT;
    }
}
