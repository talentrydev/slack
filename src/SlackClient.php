<?php

declare(strict_types=1);

namespace Talentry\Slack;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Utils;
use LogicException;
use Psr\Http\Message\ResponseInterface;
use SplFileInfo;
use Talentry\Slack\Exception\SlackApiException;

class SlackClient
{
    private const string DEFAULT_CHANNEL = 'general';

    public function __construct(
        private readonly string $slackApiToken,
        private readonly ClientInterface $guzzleClient = new Client(),
    ) {
    }

    public function send(Message $message, ?Channel $channel = null): void
    {
        $channel = $channel ?? $this->getDefaultChannel();
        $jsonData = [
            'channel' => $channel->id(),
            'type' => $message->type()->value,
            'text' => $message->text(),
        ];

        $this->makeRequest(
            method: 'POST',
            url: Resource::POST_MESSAGE->url(),
            jsonData: $jsonData,
        );
    }

    public function sendPlainTextMessage(string $message, ?Channel $channel = null): void
    {
        $this->send(new Message(MessageType::TEXT, $message), $channel);
    }

    public function sendMarkdownMessage(string $message, ?Channel $channel = null): void
    {
        $this->send(new Message(MessageType::MARKDOWN, $message), $channel);
    }

    public function sendFile(SplFileInfo $file, ?Channel $channel = null, ?string $message = null): void
    {
        $response = $this->makeRequest(
            method: 'GET',
            url: Resource::FILES_GET_UPLOAD_URL->url(),
            queryParams: [
                'length' => $file->getSize(),
                'filename' => $file->getBasename(),
            ],
        );

        $responseString = $response->getBody()->getContents();
        $responseData = json_decode($responseString, true, 512, JSON_THROW_ON_ERROR);
        if ($responseData['ok'] !== true || !isset($responseData['upload_url']) || !isset($responseData['file_id'])) {
            throw new SlackApiException('Invalid Slack API response: ' . $responseString);
        }

        $uploadUrl = $responseData['upload_url'];
        $fileId = $responseData['file_id'];

        $this->makeRequest(
            method: 'POST',
            url: $uploadUrl,
            files: [$file],
        );

        $jsonData = [
            'files' => [
                ['id' => $fileId],
            ],
            'channel_id' => ($channel ?? $this->getDefaultChannel())->id(),
        ];

        if ($message !== null) {
            $jsonData['initial_comment'] = $message;
        }

        $response = $this->makeRequest(
            method: 'POST',
            url: Resource::FILES_COMPLETE_UPLOAD->url(),
            jsonData: $jsonData,
        );

        $responseString = $response->getBody()->getContents();
        $responseData = json_decode($responseString, true, 512, JSON_THROW_ON_ERROR);
        if ($responseData['ok'] !== true) {
            throw new SlackApiException('Invalid Slack API response: ' . $responseString);
        }
    }

    public function getChannel(string $channelName): ?Channel
    {
        return $this->getChannelUsingPagination($channelName);
    }

    public function getDefaultChannel(): Channel
    {
        $channel = $this->getChannel(self::DEFAULT_CHANNEL);
        if ($channel === null) {
            throw new LogicException('Default slack channel does not exist');
        }

        return $channel;
    }

    /**
     * @param array<string,mixed> $jsonData
     * @param array<string,string> $queryParams
     * @param SplFileInfo[] $files,
     */
    private function makeRequest(
        string $method,
        string $url,
        array $jsonData = [],
        array $queryParams = [],
        array $files = [],
    ): ResponseInterface {
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->slackApiToken,
            ],
        ];

        if (count($queryParams) > 0) {
            $url .= '?' . http_build_query($queryParams);
        }

        if (count($jsonData) > 0) {
            $options['json'] = $jsonData;
        }

        if (count($files) > 0) {
            $options['multipart'] = [];
            foreach ($files as $file) {
                $options['multipart'][] = [
                    'name' => $file->getBasename(),
                    'contents' => Utils::tryFopen($file->getRealPath(), 'r'),
                ];
            }
        }

        return $this->guzzleClient->request($method, $url, $options);
    }

    private function getChannelUsingPagination(string $channelName, ?string $cursor = null): ?Channel
    {
        $queryParams = [];
        if ($cursor !== null) {
            $queryParams['cursor'] = $cursor;
        }

        $responseObject = $this->makeRequest(
            method: 'GET',
            url: Resource::LIST_CHANNELS->url(),
            queryParams: $queryParams,
        );
        $responseString = $responseObject->getBody()->getContents();
        $response = json_decode($responseString, true, 512, JSON_THROW_ON_ERROR);
        if (!array_key_exists('ok', $response) || !$response['ok'] || !array_key_exists('channels', $response)) {
            throw new SlackApiException('Invalid Slack API response: ' . $responseString);
        }

        foreach ($response['channels'] as $channel) {
            if ($channel['name'] === $channelName) {
                return new Channel($channel['id'], $channel['name']);
            }
        }

        if (
            array_key_exists('response_metadata', $response) &&
            array_key_exists('next_cursor', $response['response_metadata']) &&
            $response['response_metadata']['next_cursor'] !== null &&
            $response['response_metadata']['next_cursor'] !== ''
        ) {
            return $this->getChannelUsingPagination($channelName, $response['response_metadata']['next_cursor']);
        }

        return null;
    }
}
