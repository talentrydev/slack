<?php

declare(strict_types=1);

namespace Talentry\Slack\Exception;

use Exception;

class SlackApiException extends Exception
{
}
