<?php

declare(strict_types=1);

namespace Talentry\Slack;

class File
{
    public function __construct(
        private readonly string $id,
        private readonly string $url,
        private readonly string $mimetype,
        private readonly string $name,
        private readonly string $title,
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function url(): string
    {
        return $this->url;
    }

    public function mimetype(): string
    {
        return $this->mimetype;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function title(): string
    {
        return $this->title;
    }
}
