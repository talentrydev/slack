<?php

declare(strict_types=1);

namespace Talentry\Slack;

class Message
{
    public function __construct(
        private readonly MessageType $type,
        private readonly string $text,
    ) {
    }

    public static function plainText(string $message): self
    {
        return new self(MessageType::TEXT, $message);
    }

    public static function markdown(string $message): self
    {
        return new self(MessageType::MARKDOWN, $message);
    }

    public function type(): MessageType
    {
        return $this->type;
    }

    public function text(): string
    {
        return $this->text;
    }
}
