<?php

declare(strict_types=1);

namespace Talentry\Slack;

enum Resource: string
{
    private const string BASE_URL = 'https://slack.com/api/';

    case LIST_CHANNELS = 'conversations.list';
    case POST_MESSAGE = 'chat.postMessage';
    case FILES_GET_UPLOAD_URL = 'files.getUploadURLExternal';
    case FILES_COMPLETE_UPLOAD = 'files.completeUploadExternal';

    public function url(): string
    {
        return self::BASE_URL . $this->value;
    }
}
